<?php

namespace Mpwar\SignUp\Infrastructure\EventHandler;


use Mpwar\SignUp\Domain\Event\Event;
use Mpwar\SignUp\Domain\EventHandler\EventHandler;

class MemoryEventHandler implements EventHandler
{

    private $events = [];

    public function handleEvent(Event $event)
    {
        $this->events[] = $event;
    }

    public function getLastEvent()
    {
        return array_shift($this->events);
    }
}