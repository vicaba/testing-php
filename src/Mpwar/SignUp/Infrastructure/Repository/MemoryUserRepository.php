<?php

namespace Mpwar\SignUp\Infrastructure\Repository;


use Mpwar\SignUp\Domain\User\Entity\User;
use Mpwar\SignUp\Domain\User\Repository\UserRepository;

class MemoryUserRepository implements UserRepository
{

    private $users = [];

    public function saveUser(User $user)
    {
        $this->users[$user->getEmail()->getAsString()] = $user->getPassword()->getAsString();
    }

    public function getUser(User $user)
    {
        if (!isset($this->users[$user->getEmail()->getAsString()])) return null;

        $retrievedPassword = $this->users[$user->getEmail()->getAsString()];

        if (strcmp($retrievedPassword, $this->users[$user->getEmail()->getAsString()]) != 0) return null;

        return $user;
    }
}