<?php

namespace Mpwar\SignUp\Domain\EventHandler;


use Mpwar\SignUp\Domain\Event\Event;

interface EventHandler
{
    public function handleEvent(Event $event);
    public function getLastEvent();

}