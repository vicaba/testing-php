<?php

namespace Mpwar\SignUp\Domain\Service;


interface LoginUser
{
    public function loginUser($email, $password);
}