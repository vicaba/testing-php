<?php

namespace Mpwar\SignUp\Domain\Service;


interface RegisterUser
{
    public function registerUser($email, $password);
}