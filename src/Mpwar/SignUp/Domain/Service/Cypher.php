<?php

namespace Mpwar\SignUp\Domain\Service;


interface Cypher
{
    public function encrypt($string);
}