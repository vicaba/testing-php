<?php

namespace Mpwar\SignUp\Domain\Service;


use Mpwar\SignUp\Domain\Event\Event;
use Mpwar\SignUp\Domain\EventHandler\EventHandler;
use Mpwar\SignUp\Domain\User\Entity\User;
use Mpwar\SignUp\Domain\User\Lang\UserEmail;
use Mpwar\SignUp\Domain\User\Lang\UserPassword;
use Mpwar\SignUp\Domain\User\Repository\UserRepository;

class LoginUserService implements LoginUser
{
    private $userRepository;

    private $autoLoginUserEvent;

    private $eventHandler;

    public function __construct(
        UserRepository $userRepository,
        Event $autoLoginUserEvent,
        EventHandler $eventHandler
    )
    {
        $this->userRepository = $userRepository;
        $this->autoLoginUserEvent = $autoLoginUserEvent;
        $this->eventHandler = $eventHandler;
    }

    public function loginUser($email, $password)
    {
        $userEmail = new UserEmail($email);
        $userPassword = UserPassword::fromUnencryptedString($password);

        $user = new User($userEmail, $userPassword);

        $retrievedUser = $this->userRepository->getUser($user);
        if (is_null($retrievedUser)) return;
        $this->eventHandler->handleEvent($this->autoLoginUserEvent);
    }
}