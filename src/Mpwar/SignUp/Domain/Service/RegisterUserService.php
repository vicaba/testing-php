<?php

namespace Mpwar\SignUp\Domain\Service;


use Mpwar\SignUp\Domain\Event\Event;
use Mpwar\SignUp\Domain\EventHandler\EventHandler;
use Mpwar\SignUp\Domain\User\Entity\User;
use Mpwar\SignUp\Domain\User\Lang\UserEmail;
use Mpwar\SignUp\Domain\User\Lang\UserPassword;
use Mpwar\SignUp\Domain\User\Repository\UserRepository;

class RegisterUserService implements RegisterUser
{
    private $userRepository;

    private $newUserEvent;

    private $eventHandler;

    private $autoLoginEvent;

    public function __construct(
        UserRepository $userRepository,
        Event $newUserEvent,
        Event $autoLoginEvent,
        EventHandler $eventHandler
    )
    {
        $this->userRepository = $userRepository;
        $this->newUserEvent = $newUserEvent;
        $this->eventHandler = $eventHandler;
        $this->autoLoginEvent = $autoLoginEvent;
    }

    public function registerUser($email, $password)
    {
        $userEmail = new UserEmail($email);
        $userPassword = UserPassword::fromUnencryptedString($password);

        $user = new User($userEmail, $userPassword);

        $this->userRepository->saveUser($user);
        $this->eventHandler->handleEvent($this->newUserEvent);
        $this->eventHandler->handleEvent($this->autoLoginEvent);

    }
}