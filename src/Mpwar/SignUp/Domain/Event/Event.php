<?php

namespace Mpwar\SignUp\Domain\Event;


interface Event
{
    public function getName();
}