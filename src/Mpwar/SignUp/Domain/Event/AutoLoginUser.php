<?php

namespace Mpwar\SignUp\Domain\Event;


class AutoLoginUser implements Event
{

    const NAME = "AutoLoginUser";

    public function getName()
    {
        return self::NAME;
    }
}