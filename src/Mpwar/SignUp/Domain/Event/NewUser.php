<?php

namespace Mpwar\SignUp\Domain\Event;


class NewUser implements Event
{

    const NAME = "NewUser";

    public function getName()
    {
        return self::NAME;
    }
}