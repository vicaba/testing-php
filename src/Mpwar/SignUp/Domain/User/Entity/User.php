<?php

namespace Mpwar\SignUp\Domain\User\Entity;


use Mpwar\SignUp\Domain\User\Lang\UserEmail;
use Mpwar\SignUp\Domain\User\Lang\UserPassword;

class User
{
    private $email;

    private $password;

    public function __construct(UserEmail $email, UserPassword $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return UserEmail
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return UserPassword
     */
    public function getPassword()
    {
        return $this->password;
    }
    
}