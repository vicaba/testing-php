<?php

namespace Mpwar\SignUp\Domain\User\Repository;


use Mpwar\SignUp\Domain\User\Entity\User;

interface UserRepository
{

    public function saveUser(User $user);

    public function getUser(User $user);

}