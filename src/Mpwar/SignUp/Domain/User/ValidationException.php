<?php

namespace Mpwar\SignUp\Domain\User;


use RuntimeException;

class ValidationException extends RuntimeException {}