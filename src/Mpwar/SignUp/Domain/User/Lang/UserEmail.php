<?php

namespace Mpwar\SignUp\Domain\User\Lang;


use Mpwar\SignUp\Domain\User\ValidationException;

class UserEmail
{

    private $email;

    public function __construct($email)
    {
        if (!$this->validate($email)) throw new ValidationException();

        $this->email = $email;
    }
    
    public function getAsString()
    {
        return (string) $this->email;
    }

    private function validate($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

}
