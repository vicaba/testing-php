<?php

namespace Mpwar\SignUp\Domain\User\Lang;


use Mpwar\SignUp\Domain\Service\Cypher;
use Mpwar\SignUp\Domain\User\ValidationException;

class UserPassword
{

    const PASSWORD_LENGTH = 6;

    private $password;

    public static function fromUnencryptedString($password)
    {
        if (!self::validate($password)) throw new ValidationException();

        return new self(self::encrypt($password));
    }

    public static function fromEncryptedString($encryptedString)
    {
        return new self($encryptedString);
    }

    private function __construct($password)
    {
        $this->password = $password;
    }

    public function getAsString()
    {
        return $this->password;
    }

    private static function validate($password)
    {

        if (strlen($password) < self::PASSWORD_LENGTH) {
            return false;
        }

        if (!preg_match('/[A-Z]/', $password)) {
            return false;
        }

        if (!preg_match('/[a-z]/', $password)) {
            return false;
        }

        return true;
    }

    private static function encrypt($rawPassword)
    {
        return sha1($rawPassword);
    }
}
