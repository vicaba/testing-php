<?php

namespace Mpwar\FizzBuzz\Solver;

use Mpwar\FizzBuzz\Solver;

final class BuzzSolver implements Solver
{
    const BUZZ_DIVIDEND = 5;
    const BUZZ_VALUE = 'buzz';

    public function composeStackedResult($inputNumber, $stackedResult)
    {
        if ($inputNumber % self::BUZZ_DIVIDEND !== 0) {
            return $stackedResult;
        }

        $formattedStackedResult = $stackedResult
            ? $stackedResult . ' '
            : '';

        return $formattedStackedResult . self::BUZZ_VALUE;
    }
}
