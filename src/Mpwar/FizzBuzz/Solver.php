<?php

namespace Mpwar\FizzBuzz;

interface Solver
{
    public function composeStackedResult($inputNumber, $stackedResult);
}