Feature: I can detect buzz numbers
    As a sane person
    I want to make sure that buzz numbers can be recognised
    So that I can pass testing

    Scenario: default case
        Given a 5
        When executing the fizz buzz service
        Then the result should be 'buzz'

    Scenario: another case
        Given a 10
        When executing the fizz buzz service
        Then the result should be 'buzz'

    Scenario: another case
        Given a 20
        When executing the fizz buzz service
        Then the result should be 'buzz'
