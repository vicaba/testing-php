<?php

namespace MpwarIntegration\FizzBuzz\Context;

use Behat\Behat\Context\SnippetAcceptingContext;
use Mpwar\FizzBuzz\FizzBuzz;
use Mpwar\FizzBuzz\Solver\FizzSolver;
use Mpwar\FizzBuzz\Solver\BuzzSolver;
use Mpwar\FizzBuzz\Solver\GenericSolver;
use PHPUnit_Framework_Assert;

final class DefaultContext implements SnippetAcceptingContext
{
    /**
     * @Given a :number
     */
    public function givenANumber($number)
    {
        $this->inputNumber = $number;
    }

    /**
     * @When executing the fizz buzz service
     */
    public function executingTheFizzBuzzService()
    {
        $fizzSolver = new FizzSolver;
        $buzzSolver = new BuzzSolver;
        $genericSolver = new GenericSolver;

        $fizzBuzz = new FizzBuzz([
            $fizzSolver,
            $buzzSolver,
            $genericSolver,
        ]);

        $this->result = $fizzBuzz->calculateResult($this->inputNumber);
    }

    /**
     * @Then /the result should be \'(.*)\'/
     */
    public function theResultShouldBe($result)
    {
        PHPUnit_Framework_Assert::assertSame(
            $result,
            $this->result
        );
    }
}
