<?php

namespace MpwarIntegration\SignUp\Context;


use Behat\Behat\Context\SnippetAcceptingContext;
use Mpwar\SignUp\Domain\Event\AutoLoginUser;
use Mpwar\SignUp\Domain\Event\NewUser;
use Mpwar\SignUp\Domain\EventHandler\EventHandler;
use Mpwar\SignUp\Domain\Service\RegisterUser;
use Mpwar\SignUp\Domain\Service\RegisterUserService;
use Mpwar\SignUp\Domain\User\Entity\User;
use Mpwar\SignUp\Domain\User\Lang\UserEmail;
use Mpwar\SignUp\Domain\User\Lang\UserPassword;
use Mpwar\SignUp\Domain\User\Repository\UserRepository;
use Mpwar\SignUp\Domain\User\ValidationException;
use Mpwar\SignUp\Infrastructure\EventHandler\MemoryEventHandler;
use Mpwar\SignUp\Infrastructure\Repository\MemoryUserRepository;
use Mpwar\SignUp\Infrastructure\Repository\UserMemoryRepository;
use PHPUnit_Framework_Assert;

final class SignUpContext implements SnippetAcceptingContext
{
    /**
     * @var UserRepository
     */
    private static $repository;

    /**
     * @var EventHandler
     */
    private static $eventHandler;

    /**
     * @var NewUser
     */
    private static $newUserEvent;

    /**
     * @var AutoLoginUser
     */
    private static $autoLoginEvent;

    /**
     * @var RegisterUser
     */
    private static $registerService;

    private static $email;

    private static $password;

    /**
     * @BeforeSuite
     */
    public static function prepare()
    {
        self::$repository = new MemoryUserRepository();
        self::$eventHandler = new MemoryEventHandler();
        self::$newUserEvent = new NewUser();
        self::$autoLoginEvent = new AutoLoginUser();
        self::$registerService = new RegisterUserService(self::$repository, self::$autoLoginEvent, self::$newUserEvent, self::$eventHandler);
    }

    /**
     * @AfterSuite
     *
     */
    public static function clean()
    {
        self::$repository = null;
        self::$eventHandler = null;
        self::$newUserEvent = null;
        self::$autoLoginEvent = null;
        self::$registerService = null;
    }

    /**
     * @Given an email :email
     */
    public function givenAnEmail($email)
    {
        self::$email = $email;
    }

    /**
     * @Given a password :password
     */
    public function givenAPassword($password)
    {
        self::$password = $password;
    }

    /**
     * @When registering
     */
    public function registering()
    {
        self::$registerService->registerUser(self::$email, self::$password);
    }

    /**
     * @Then the database has the user
     */
    public function theDatabaseHasTheUser()
    {
        $user = new User(new UserEmail(self::$email), UserPassword::fromUnencryptedString(self::$password));

        $retrievedUser = self::$repository->getUser($user);

        PHPUnit_Framework_Assert::assertInstanceOf(User::class, $retrievedUser, "a user should be returned");
        PHPUnit_Framework_Assert::assertSame(self::$email, $user->getEmail()->getAsString(), "the retrieved user email is the same");
        PHPUnit_Framework_Assert::assertSame(
            UserPassword::fromUnencryptedString(self::$password)->getAsString(), $user->getPassword()->getAsString(),
            "the retrieved user password is the same"
        );
    }

    /**
     * @Then a NewUser Event and a AutoLoginUser Event are triggered
     */
    public function aNewUserEventAndAAutoLoginUserEventAreTriggered()
    {

        $events = [];

        $event = self::$eventHandler->getLastEvent();

        if (!is_null($event)) {
            $events[] = $event->getName();
        }

        $event = self::$eventHandler->getLastEvent();

        if (!is_null($event)) {
            $events[] = $event->getName();
        }

        PHPUnit_Framework_Assert::assertTrue(in_array(NewUser::NAME, $events), "a NewUser event should be triggered");
        PHPUnit_Framework_Assert::assertTrue(in_array(AutoLoginUser::NAME, $events), "a AutoLoginUser event should be triggered");

    }

    /**
     * @Given an invalid email :email
     */
    public function givenAnInvalidEmail($email)
    {
        self::$email = $email;
    }

    /**
     * @Given an invalid password :password
     */
    public function givenAnInvalidPassword($password)
    {
        self::$password = $password;
    }

    /**
     * @When registering an exception should be thrown
     */
    public function whenRegisteringAnExceptionShouldBeThrown()
    {
        $exceptionThrown = false;

        try {
            $this->registering();
        } catch (ValidationException $e) {
            $exceptionThrown = true;
        } finally {
            if ($exceptionThrown) PHPUnit_Framework_Assert::assertTrue($exceptionThrown, "An exception was not thorwn");
        }
    }

}