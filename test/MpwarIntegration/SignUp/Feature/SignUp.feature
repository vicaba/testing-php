Feature: I can register
  As a web user
  I want to register myself to the application
  So that I can have signed access to the application

  Scenario: I can register to the application if a user with the same email does not exists and the password is correct
    Given an email "valid@valid.com"
    And   a password "ValidP"
    When  registering
    Then  the database has the user
    And   a NewUser Event and a AutoLoginUser Event are triggered
  Scenario: I can't register to the application if the email is invalid
    Given an invalid email "validvalid.com"
    And   a password "ValidP"
    When  registering an exception should be thrown
  Scenario: I can't register to the application if the password is invalid
    Given an email "valid@valid.com"
    And   an invalid password "Valid"
    When  registering an exception should be thrown