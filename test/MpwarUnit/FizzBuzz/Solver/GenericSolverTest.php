<?php

namespace MpwarUnit\FizzBuzz\Solver;

use Mpwar\FizzBuzz\Solver\GenericSolver;
use PHPUnit_Framework_TestCase;

final class GenericSolverTest extends PHPUnit_Framework_TestCase
{
    const NUMBER = 24;
    const FILLED_UP_STACKED_RESULT = 'monesvol';
    const EMPTY_STACKED_RESULT = '';

    /** @test */
    public function shouldReturnTheStackedResult()
    {
        $this->givenAFilledUpStackedResult();
        $this->thenTheResultWillBeTheStackedResultOnly();
    }

    /** @test */
    public function shouldReturnTheNumberAsAString()
    {
        $this->givenAnEmptyStackedResult();
        $this->thenTheResultWillBeTheGivenNumberAsAString();
    }

    private function givenAFilledUpStackedResult()
    {
        $this->stackedResult = self::FILLED_UP_STACKED_RESULT;
    }

    private function givenAnEmptyStackedResult()
    {
        $this->stackedResult = self::EMPTY_STACKED_RESULT;
    }

    private function thenTheResultWillBeTheStackedResultOnly()
    {
        $result = $this->executeService();
        $this->assertSame($this->stackedResult, $result);
    }

    private function thenTheResultWillBeTheGivenNumberAsAString()
    {
        $result = $this->executeService();
        $this->assertSame((string) self::NUMBER, $result);
    }

    private function executeService()
    {
        $genericSolver = new GenericSolver;

        return $genericSolver->composeStackedResult(
            self::NUMBER,
            $this->stackedResult
        );
    }
}