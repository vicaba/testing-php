<?php

namespace MpwarUnit\SignUp\Domain\User\Lang;


use Mpwar\SignUp\Domain\User\Lang\UserPassword;
use Mpwar\SignUp\Domain\User\ValidationException;

class UserPasswordTest extends \PHPUnit_Framework_TestCase
{

    private $rawPassword;

    private $userPassword;

    protected function tearDown()
    {
        $this->rawPassword = null;
        $this->userPassword = null;
    }

    /**
     * @test
     * @dataProvider validPasswordProvider
     */
    public function shouldAUserPasswordBeCreatedWhenPasswordIsValid($password)
    {
        $this->givenAValidPassword($password);
        $this->thenAUserPasswordShouldBeCreated();
    }

    /**
     * @test
     * @dataProvider invalidPasswordProvider
     */
    public function shouldAnExceptionBeThrownWhenTryingToCreateAUserPasswordWithAnInvalidPassword($password)
    {
        $this->givenAnInvalidPassword($password);
        $this->thenAnExceptionShouldBeThrownWhenTryingToCreateAUserPassword();
    }

    /**
     * @test
     * @dataProvider validPasswordProvider
     */
    public function shouldAUserPasswordContainAnEncryptedPasswordIfBuiltFromAnUnencryptedString($password)
    {
        $this->givenAValidPassword($password);
        $this->thenAUserPasswordShouldBeCreatedAndThePasswordShouldBeEqualAsTheEncryptedRawPassword($password);
    }

    public function validPasswordProvider()
    {
        return [
            ["abcdeFM"],
            ["    Aa"],
            ["aaaAAA"],
            ["aA aA "]
        ];
    }

    public function invalidPasswordProvider()
    {
        return [
            [""],
            [" d"],
            ["      "],
            ["aaaaaa"],
            ["AAAAAA"],
            ["mmaAm"]
        ];
    }

    private function givenAValidPassword($password)
    {
        $this->rawPassword = $password;
    }

    private function thenAUserPasswordShouldBeCreated()
    {
        $this->createUserPasswordObjectFromRawPassword();
        $this->assertInstanceOf(UserPassword::class, $this->userPassword);
    }

    private function givenAnInvalidPassword($password)
    {
        $this->rawPassword = $password;
    }

    private function thenAnExceptionShouldBeThrownWhenTryingToCreateAUserPassword()
    {
        $this->expectException(ValidationException::class);
        $this->createUserPasswordObjectFromRawPassword();
    }

    private function thenAUserPasswordShouldBeCreatedAndThePasswordShouldBeEqualAsTheEncryptedRawPassword($password)
    {
        $this->createUserPasswordObjectFromRawPassword();
        $encryptedPassword = sha1($password);
        $this->assertSame($encryptedPassword, $this->userPassword->getAsString(), "Encrypted passwords should be equal");
    }

    private function createUserPasswordObjectFromRawPassword()
    {
        $this->userPassword = UserPassword::fromUnencryptedString($this->rawPassword);
    }

}