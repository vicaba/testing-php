<?php

namespace MpwarUnit\SignUp\Domain\User\Lang;


use Mpwar\SignUp\Domain\User\Lang\UserEmail;
use Mpwar\SignUp\Domain\User\ValidationException;

class UserEmailTest extends \PHPUnit_Framework_TestCase
{

    private $rawEmail;

    private $userEmail;

    protected function tearDown()
    {
        $this->rawUserEmail = null;
        $this->userEmail = null;
    }

    /**
     * @test
     * @dataProvider validEmailProvider
     */
    public function shouldAUserEmailBeCreatedWhenPasswordIsValid($email)
    {
        $this->givenAValidEmail($email);
        $this->thenAUserEmailShouldBeCreated();
    }

    /**
     * @test
     * @dataProvider invalidEmailProvider
     */
    public function shouldAnExceptionBeThrownWhenTryingToCreateAUserPasswordWithAnInvalidEmail($password)
    {
        $this->givenAnInvalidEmail($password);
        $this->thenAnExceptionShouldBeThrownWhenTryingToCreateAUserEmail();
    }

    public function validEmailProvider()
    {
        return [
            ["me@example.com"],
            ["a.nonymous@example.com"],
            ["name+tag@example.com"],
            ["!#$%&'+-/=.?^`{|}~@[1.0.0.127]"],
            ["!#$%&+-/=.?^`{|}~@[IPv6:0123:4567:89AB:CDEF:0123:4567:89AB:CDEF]"]
        ];
    }

    public function invalidEmailProvider()
    {
        return [
            ["me@@example.com"],
            ["a@nonymous@example.com"],
            ["first.last@sub.do,com"],
            ["first.last@[12.34.56.789]"],
            ["first.last@-xample.com"]
        ];
    }

    private function givenAValidEmail($email)
    {
        $this->rawEmail = $email;
    }

    private function thenAUserEmailShouldBeCreated()
    {
        $this->createUserEmailObjectFromRawEmail();
        $this->assertInstanceOf(UserEmail::class, $this->userEmail);
    }

    private function givenAnInvalidEmail($email)
    {
        $this->rawEmail = $email;
    }

    private function thenAnExceptionShouldBeThrownWhenTryingToCreateAUserEmail()
    {
        $this->expectException(ValidationException::class);
        $this->createUserEmailObjectFromRawEmail();
    }

    private function createUserEmailObjectFromRawEmail()
    {
        $this->userEmail = new UserEmail($this->rawEmail);
    }

}