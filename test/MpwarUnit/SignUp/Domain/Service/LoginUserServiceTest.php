<?php

namespace MpwarUnit\SignUp\Domain\Service;


use Mpwar\SignUp\Domain\Event\Event;
use Mpwar\SignUp\Domain\EventHandler\EventHandler;
use Mpwar\SignUp\Domain\Service\Cypher;
use Mpwar\SignUp\Domain\Service\LoginUserService;
use Mpwar\SignUp\Domain\User\Repository\UserRepository;

class LoginUserServiceTest extends \PHPUnit_Framework_TestCase
{
    const VALID_EMAIL = "a@a.com";

    const VALID_PASSWORD = "aaaaaA";

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $userRepository;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $event;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $eventHandler;

    protected function setUp()
    {
        $this->userRepository = $this->getMock(UserRepository::class);
        $this->event = $this->getMock(Event::class);
        $this->eventHandler = $this->getMock(EventHandler::class);
    }

    /**
     * @test
     */
    public function shouldCallTheUserRepository()
    {
        $this->givenAUserRepository();
        $this->thenTheRepositoryShouldBeQueriedWhenAUserIsLoggingIn();
    }

    /**
     * @test
     */
    public function shouldTriggerAnEvent()
    {
        $this->givenAnEventHandler();
        $this->givenAUserRepositoryThatReturnsTheUser();
        $this->thenThenTheRepositoryShouldBeQueriedAndAnEventShouldBeTriggeredWhenAUserIsLoggingIn();
    }

    private function givenAUserRepository()
    {
        $this->userRepository
            ->expects($this->once())
            ->method('getUser');
    }

    private function thenTheRepositoryShouldBeQueriedWhenAUserIsLoggingIn()
    {
        $this->execute();
    }

    private function givenAUserRepositoryThatReturnsTheUser()
    {
        $this->userRepository
            ->expects($this->once())
            ->method('getUser')
            ->willReturn(true);
    }

    private function givenAnEventHandler()
    {
        $this->eventHandler
            ->expects($this->once())
            ->method('handleEvent');
    }

    private function thenThenTheRepositoryShouldBeQueriedAndAnEventShouldBeTriggeredWhenAUserIsLoggingIn()
    {
        $this->execute();
    }

    private function execute()
    {
        $registerUserService = new LoginUserService($this->userRepository, $this->event, $this->eventHandler);
        $registerUserService->loginUser(self::VALID_EMAIL, self::VALID_PASSWORD);
    }

}