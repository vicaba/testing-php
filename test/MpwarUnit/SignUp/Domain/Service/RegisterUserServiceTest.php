<?php

namespace MpwarUnit\SignUp\Domain\Service;


use Mpwar\SignUp\Domain\Event\Event;
use Mpwar\SignUp\Domain\EventHandler\EventHandler;
use Mpwar\SignUp\Domain\Service\Cypher;
use Mpwar\SignUp\Domain\Service\RegisterUserService;
use Mpwar\SignUp\Domain\User\Repository\UserRepository;

class RegisterUserServiceTest extends \PHPUnit_Framework_TestCase
{

    const VALID_EMAIL = "a@a.com";

    const VALID_PASSWORD = "aaaaaA";

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $userRepository;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $newUserEvent;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $autoLoginEvent;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $eventHandler;

    protected function setUp()
    {
        $this->userRepository = $this->getMock(UserRepository::class);
        $this->newUserEvent = $this->getMock(Event::class);
        $this->autoLoginEvent = $this->getMock(Event::class);
        $this->eventHandler = $this->getMock(EventHandler::class);
    }

    /**
     * @test
     */
    public function shouldSaveUserIntoTheDatabase()
    {
        $this->givenAUserRepository();
        $this->thenAUserShouldBeSavedInTheDatabaseWhenAUserIsRegistered();
    }

    /**
     * @test
     */
    public function shouldTriggerTwoEvents()
    {
        $this->givenAnEventHandler();
        $this->thenTwoEventsShouldBeTriggeredWhenAUserIsRegistered();
    }

    private function givenAUserRepository()
    {
        $this->userRepository
            ->expects($this->once())
            ->method('saveUser');
    }

    private function thenAUserShouldBeSavedInTheDatabaseWhenAUserIsRegistered()
    {
        $this->execute();
    }

    private function givenAnEventHandler()
    {
        $this->eventHandler
            ->expects($this->exactly(2))
            ->method('handleEvent');
    }

    private function thenTwoEventsShouldBeTriggeredWhenAUserIsRegistered()
    {
        $this->execute();
    }

    private function execute()
    {
        $registerUserService =
            new RegisterUserService($this->userRepository, $this->newUserEvent, $this->autoLoginEvent, $this->eventHandler);
        $registerUserService->registerUser(self::VALID_EMAIL, self::VALID_PASSWORD);
    }

}